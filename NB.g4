grammar NB;

options {
	language = Java;
}

@header {
    package Parser;
}

//============================================================================================================
//Parse Rule:

//Top Level Rule
file:
    (line)*
    ;

line:
    expression (PERIOD)? NEWLINE								#StartLine
    ;

expression:
	expression expression									#Recursion
    |	CANCEL_START expression CANCEL_END							#Cancellation	//expression is crossed out
    |	(SPACE)* expression (SPACE)*								#Spaces
    |	expression ASTERISK									#Asterisk
    |	LEFT_BRACE expression RIGHT_BRACE							#Braces
    |   BOLD_LEFT_BRACKET expression BOLD_RIGHT_BRACKET						#BoldfaceBrackets
    |   LEFT_BRACKET expression RIGHT_BRACKET							#Brackets
    |   LEFT_PAREN expression RIGHT_PAREN							#Parentheses
    |   ANGLE_BRACKET_L expression ANGLE_BRACKET_R						#AngleBrackets
    |	HALF_BRACKET_UPPER_L expression HALF_BRACKET_UPPER_R					#Ceiling
    |	HALF_BRACKET_LOWER_L expression HALF_BRACKET_LOWER_R					#Floor
    |	expression COMMA SPACE									#List		//I replaced Function with this
    |   INTEGRAL expression									#Integral
    |   expression EXPONENT_START expression (BASE_LINE)?					#Exponent
    |   expression SUBSCRIPT_START expression (BASE_LINE)?					#Subscript
    |	BASE_LINE expression									#ReturnToBase
    |   VARIABLE NUMBER										#ImplicitSubscript
    |   expression NOT										#Not
    |   expression COMPLEMENT									#Complement
    |   expression FACTORIAL									#Factorial
    |   (OPT_ROOT_START expression)? SQ_ROOT_START expression SQ_ROOT_END			#Root
    |   (OPT_ROOT_START expression)? SQ_ROOT_START expression					#AltRoot	//No bar over expression
    |   expression (MULTIPLY | DIVIDE | MODULO | CARTESIAN_PRODUCT) expression			#HighPrecedenceOp
    |   NUMBER VARIABLE										#ImplicitMultiplication
    |   expression (PLUS | MINUS | PLUS_OVER_MINUS | MINUS_OVER_PLUS 
			| BACK_SLASH | PLUS_MINUS | MINUS_PLUS) expression			#LowPrecedenceOp
    |   expression (LESS | LESS_EQUAL | GREATER | GREATER_EQUAL) expression			#Relational
    |   expression (EQUALITY | NOTEQUALS | APPROXIMATELY) expression				#Equality
    |   expression (SIMILAR | CONGRUENT | NOTCONGRUENT | PROPORTION | RATIO) expression		#GeometricEquality
    |   expression (PERPENDICULAR | PARALLEL) expression					#GeometricLineRelation
    |	TILDE expression									#Tilde
    |   expression (CONJUNCTION | INTERSECTION | AMPERSAND) expression				#And
    |   expression (UNION | VEE) expression							#Or
    |   expression (SUBSET | SUPERSET | SUBSET_OR_EQUAL | SUPERSET_OR_EQUAL) expression		#SetRelational
    |   expression (MEMBERSHIP | REV_MEMBERSHIP) expression					#SetMembership
    |   TRIG (EXPONENT_START expression)? SPACE expression					#Trigonometry
    |	LOG ((SUBSCRIPT_START)? expression)? SPACE expression					#Logarithm
    |	SPECIAL_FUNCTION									#SpecialFunction	//Treat the same as variable
    |	BOLD_VERTICAL_BAR									#BoldfaceVerticalBar
    |	VERTICAL_BAR										#VerticalBar
    |	FRACTION_START expression (VERT_FRACTION) expression FRACTION_END			#VerticalFraction
    |	expression (HORIZ_FRACTION) expression							#HorizontalFraction
    |	NUMBER (MIXED_START) NUMBER (VERT_FRACTION) NUMBER (MIXED_END)				#VerticalMixedNumber
    |	NUMBER (MIXED_START) NUMBER (HORIZ_FRACTION) NUMBER (MIXED_END)				#HorizontalMixedNumber
    |   NUMBER DEGREES										#Degrees
    |   DOLLARS NUMBER										#Dollars
    |   NUMBER CENTS										#Cents
    |	TRIANGLE VARIABLE									#Triangle
    |	NUMBER (SPACE)? REMAINDER NUMBER							#Remainder
    |   NUMBER											#Number
    |	DOT VARIABLE										#GreekLetter
    |   VARIABLE										#Variable
    |   INFINITY										#Infinity
    |   E											#E
    |   OMISSION										#QuestionMark
    |   UNDERSCORES										#FillInTheBlank
    |	ELLIPSIS										#Ellipsis
    |	NUMBER_SIGN										#NumberSign
    |   											#NoExpression
    ;




//============================================================================================================
//Lexer Rules:

//Relational Operators:
NOT			: '\'';
NOTEQUALS		: '/.K';
EQUALITY		: '.K' | '|.K';
GREATER			: '.1' | '..1';
GREATER_EQUAL		: '.1:' | ':.1' | '.K.1' | '.1.K';
LESS			: '"K' | '."K';
LESS_EQUAL		: '"K:' | ':"K' | '.K"K' | '"K.K';

//Geometric Operators:
APPROXIMATELY		: '@:@:';
SIMILAR			: '@:';
CONGRUENT		: '@:.K' | '_L';
NOTCONGRUENT		: '/@:.K';
PERPENDICULAR		: '$P';
PARALLEL		: '$L';
PROPORTION		: ';2';			// :: 
RATIO			: '"1';			// :

//Basic Operators:
PLUS			: '+';
MINUS			: '-';
PLUS_MINUS		: ('_')?'+"'('_')?'-';
MINUS_PLUS		: ('_')?'-"'('_')?'+';
PLUS_OVER_MINUS		: '+-';
MINUS_OVER_PLUS		: '-+';
BACK_SLASH		: '_*';			//This means x divides y
MULTIPLY		: '*';			//Dot
CARTESIAN_PRODUCT	: '@*';
DIVIDE			: './';
MODULO			: '@0';
TILDE			: '@:' | '@,:';
AMPERSAND		: '_&';
CONJUNCTION		: '@%';
UNION			: '.+';
VEE			: '@+';
INTERSECTION		: '.%';
COMPLEMENT		: ':';			//This is shown as a bar over a variable
REMAINDER		: 'R''';

//Trig Operators:
TRIG			: 'SIN' | 'COS' | 'TAN'
			| 'CSC' | 'SEC' | 'COT' | 'CTN'
			| 'ARCSIN' | 'ARCCOS' | 'ARCTAN'
			| 'ARCCSC' | 'ARCSEC' | 'ARCCOT'
			| 'SINH' | 'COSH' | 'TANH'
			| 'CSCH' | 'SECH' | 'COTH' | 'CTNH';

//Logarithms:
LOG			: 'LOG' | 'ANTILOG' | 'COLOG' | 'LN';

//Other Function Names:
SPECIAL_FUNCTION	: 'AMP' | 'ARC' | 'ARG'
			| 'COVERS' | 'DET' | 'ERF'
			| 'EXP' | 'EXSEC' | 'GRAD'
			| 'HAV' | 'IM' | 'INF'
			| ('<' | '%')? 'LIM'
			| 'MAX' | 'MIN' | 'MOD'
			| 'RE' | 'SUP' | 'VERS';

//Special Operators:
VERTICAL_BAR		: '\' | ',\';
BOLD_VERTICAL_BAR	: '_\';
FACTORIAL		: '&';
INTEGRAL		: '!';
FRACTION_START		: (',')*'?';		//The commas are for complex fractions.
FRACTION_END		: (',')*'#';		//1 comma = complex and 2 = hypercomplex.
VERT_FRACTION		: (',')*'/';		//These possibly need their own parse rules.
HORIZ_FRACTION		: (',')*'_/';
MIXED_START		: '_?';
MIXED_END		: '_#';
SQ_ROOT_START		: ('.')*'>';		//The dots are for nesting
SQ_ROOT_END		: ('.')*']';
OPT_ROOT_START		: '<';
EXPONENT_START		: '^' | '^^' | '^^^'
			| ';^^' | '^;^' | ';;^'
			| ';^';
SUBSCRIPT_START		: ';' | '^;' | '^^;'
			| '^;;' | ';^;' | ';;'
			| ';;;';
BASE_LINE		: '"';			//This is optional for exponents
CANCEL_START		: '[';
CANCEL_END		: ']';

//Brackets:
LEFT_PAREN		: '(';
LARGE_LEFT_PAREN	: ',(';
RIGHT_PAREN		: ')' | ',)';
LEFT_BRACKET		: '@(' | '@,(';
BOLD_LEFT_BRACKET	: '_@(';
RIGHT_BRACKET		: '@)' | '@,)';
BOLD_RIGHT_BRACKET	: '_@)';
LEFT_BRACE		: '.(' | '.,(';
RIGHT_BRACE		: '.)' | '.,)';
ANGLE_BRACKET_L		: '..(' | '..,(';
ANGLE_BRACKET_R		: '..)' | '..,)';
HALF_BRACKET_UPPER_L	: '@^(' | '@^,(';
HALF_BRACKET_UPPER_R	: '@^)' | '@^,)';
HALF_BRACKET_LOWER_L	: '@;(' | '@;,(';
HALF_BRACKET_LOWER_R	: '@;)' | '@;,)';

//Set Notation:
MEMBERSHIP      	: '@E';
REV_MEMBERSHIP  	: '@5';
SUBSET          	: '_"K';
SUPERSET        	: '_.1';
SUBSET_OR_EQUAL 	: '_"K:';
SUPERSET_OR_EQUAL	: '_.1:';

//Constants:
INFINITY		: ',=';
E			: ';E';

//Symbols:
DEGREES			: '^.*'('"')?;
DOLLARS			: '@S';
CENTS			: '@C';
TRIANGLE		: '$t';
ASTERISK		: '@#';
NUMBER_SIGN		: '.#';

//Miscellaneous:
SPACE			: ' ';
COMMA			: ',';
DOT			: '.';
PERIOD			: '_4';
OMISSION        	: '=';
ELLIPSIS		: ''''';
UNDERSCORES     	: ('"')?'----';		// " means bold line
NEWLINE			: '\n';

NUMBER			: ('#')?('0'..'9')+'.'('0'..'9')+
			| ('#')?('0'..'9')+'."'
                	| ('#')?('0'..'9')+
            		| ('#')?'.'('0'..'9')+;
			| ('#')?('0'..'9')+(','('0'..'9')+)+
			| ('#')?('0'..'9')+(','('0'..'9')+)+'.'('0'..'9')+
VARIABLE		: (',')?('A'..'Z')*;
			|',,'('A'..'Z')*;	//Entire word is capitalized (Roman Numerals)
